# **Daily Report (2023/07/17)**  

## O
1. RESTful :Through the morning course, I learned RESTful in detail. In fact, I have also used RESTful in previous projects, but the way it is used is not very standardized, which results in many layers of URLs for front-end requests. In future practical development, I should pay attention to this point and not only use POST and GET methods.
 2. Pair Programming:This is a very new development model for me, and in previous development, I was solely responsible for a portion of the requirements. Through the afternoon experience, I found that Pair Programming can help us write more elegant code.
 3. Spring Boot:Using spring boot to contact RESTful, while familiarizing myself with RESTful, I also reviewed the syntax of spring boot.

## R
meaningful
## I
 - I am still not very familiar with some return parameters of the HTTP protocol.
 - When using spring boot development, I am still not familiar with some annotations of the Controller, and I need to consult information during the code writing process.
 - The most meaningful activity is RESTful.
## D
  Standardize the use of RESTful