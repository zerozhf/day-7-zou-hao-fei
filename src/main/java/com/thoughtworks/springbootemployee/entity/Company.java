package com.thoughtworks.springbootemployee.entity;

public class Company {
    private Long companyId;
    private String name;

    public Company(Long companyId, String name) {
        this.companyId = companyId;
        this.name = name;
    }

    public Company() {
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
