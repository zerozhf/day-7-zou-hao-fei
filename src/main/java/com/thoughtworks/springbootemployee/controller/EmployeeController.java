package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeMemoryRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employees")
public class EmployeeController {

    private static final EmployeeMemoryRepository employeeMemoryRepository = new EmployeeMemoryRepository();

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Employee createEmployee(@RequestBody Employee employee) {
        return employeeMemoryRepository.createEmployee(employee);
    }

    @GetMapping
    public List<Employee> getEmployees() {
        return employeeMemoryRepository.getEmployees();
    }

    @GetMapping("/{id}")
    public Employee getEmployeeByEmployeeId(@PathVariable Long id) {
        return employeeMemoryRepository.getEmployeeByEmployeeId(id);
    }

    @GetMapping(params = {"gender"})
    public List<Employee> getEmployeesByGender(@RequestParam String gender) {
        return employeeMemoryRepository.getEmployeesByGender(gender);
    }

    @PutMapping("/{id}")
    public Employee updateEmployeeAgeAndSalary(@PathVariable Long id, @RequestBody Employee employee) {
        return employeeMemoryRepository.updateEmployeeAgeAndSalary(id,employee);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEmployee(@PathVariable Long id) {
        employeeMemoryRepository.deleteEmployee(id);

    }

    @GetMapping(params = {"page","size"})
    public List<Employee> getEmployeesByPageAndSize(@RequestParam Integer page,Integer size) {
        return employeeMemoryRepository.getEmployeesByPageAndSize(page, size);
    }
}
